# Invoked from goreleaser, uses binaries build by goreleaser
FROM alpine:3.21
ENTRYPOINT ["/usr/local/bin/cmbr"]
COPY cmbr /usr/local/bin
