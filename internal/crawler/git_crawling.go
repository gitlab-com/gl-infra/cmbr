package crawler

import (
	"fmt"
	"net/url"
	"strings"

	colly "github.com/gocolly/colly/v2"
)

func (f *crawler) checkForRepoAPI(c *colly.Collector, baseURL *url.URL, d map[string]interface{}) {
	projectRepoURL := getStringFromJSONMap(d, "http_url_to_repo")
	if !strings.Contains(projectRepoURL, ".git") {
		return
	}

	projectWebURL := getStringFromJSONMap(d, "web_url")
	pathWithNamespace := getStringFromJSONMap(d, "path_with_namespace")

	gitUploadURL, err := url.Parse(fmt.Sprintf("%s.git/info/refs?service=git-upload-pack", projectWebURL))
	if err != nil {
		return
	}

	gitWikiUploadURL, err := url.Parse(fmt.Sprintf("%s.wiki.git/info/refs?service=git-upload-pack", projectWebURL))
	if err != nil {
		return
	}

	gitReceiveURL, err := url.Parse(fmt.Sprintf("%s.git/info/refs?service=git-receive-pack&repository_path=%s", projectWebURL, pathWithNamespace))
	if err != nil {
		return
	}

	f.addLink(c, gitUploadURL)
	f.addLink(c, gitWikiUploadURL)
	f.addLink(c, gitReceiveURL)
}
