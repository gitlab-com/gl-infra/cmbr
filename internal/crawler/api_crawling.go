package crawler

import (
	"encoding/json"
	"fmt"
	"mime"
	"net/url"

	colly "github.com/gocolly/colly/v2"
)

func getStringFromJSONMap(u interface{}, key string) string {
	if u == nil {
		return ""
	}

	m, ok := u.(map[string]interface{})
	if !ok {
		return ""
	}

	vu, ok := m[key]
	if !ok {
		return ""
	}

	switch v := vu.(type) {
	case string:
		return v
	case float64:
		return fmt.Sprintf("%.f", v)
	default:
		return ""
	}
}

func (f *crawler) checkForNamespaceAPI(c *colly.Collector, baseURL *url.URL, d map[string]interface{}) {
	kind := getStringFromJSONMap(d["namespace"], "kind")
	if kind != "group" {
		return
	}

	parentID := getStringFromJSONMap(d["namespace"], "parent_id")
	if parentID == "" {
		return
	}

	f.addRelative(c, baseURL, "/api/v4/groups/"+parentID)
	f.addRelative(c, baseURL, "/api/v4/groups/"+parentID+"/epics")
	f.addRelative(c, baseURL, "/api/v4/groups/"+parentID+"/wikis")
	f.addRelative(c, baseURL, "/api/v4/groups/"+parentID+"/boards")
	f.addRelative(c, baseURL, "/api/v4/groups/"+parentID+"/iterations")
	f.addRelative(c, baseURL, "/api/v4/groups/"+parentID+"/members")
}

func (f *crawler) navigateUser(c *colly.Collector, baseURL *url.URL, userID string) {
	if userID == "" {
		return
	}

	f.addRelative(c, baseURL, "/api/v4/users/"+userID)
	f.addRelative(c, baseURL, "/api/v4/users/"+userID+"/events")
}

func (f *crawler) checkForAuthorAPI(c *colly.Collector, baseURL *url.URL, d map[string]interface{}) {
	f.navigateUser(c, baseURL, getStringFromJSONMap(d["author"], "id"))
	f.navigateUser(c, baseURL, getStringFromJSONMap(d["assignee"], "id"))
}

func (f *crawler) crawlAPIDoc(c *colly.Collector, baseURL *url.URL, d interface{}) {
	s, ok := d.(string)
	if ok {
		u, err := url.Parse(s)
		if err == nil {
			f.addLink(c, u)
		}

		return
	}

	m, ok := d.(map[string]interface{})
	if ok {
		// Look for special values
		if f.options.Traffic == api {
			f.checkForNamespaceAPI(c, baseURL, m)
			f.checkForAuthorAPI(c, baseURL, m)
		}

		if f.options.Traffic == git {
			f.checkForRepoAPI(c, baseURL, m)
		}

		if f.options.Traffic == registry {
			f.checkForRegistryRepoAPI(c, baseURL, m)
		}

		// Traverse the values
		for _, v := range m {
			f.crawlAPIDoc(c, baseURL, v)
		}

		return
	}

	// Try treat as an array
	a, ok := d.([]interface{})
	if !ok {
		return
	}

	for _, v := range a {
		f.crawlAPIDoc(c, baseURL, v)
	}
}

func (f *crawler) traverseJSON(c *colly.Collector, r *colly.Response) {
	if len(r.Body) == 0 {
		return
	}

	mediatype, _, err := mime.ParseMediaType(r.Headers.Get("content-type"))

	if err != nil {
		return
	}

	if mediatype != "application/json" {
		return
	}

	if r.Body[0] == '{' {
		jsonMap := make(map[string]interface{})

		err := json.Unmarshal(r.Body, &jsonMap)
		if err == nil {
			f.crawlAPIDoc(c, r.Request.URL, jsonMap)
		}
	} else if r.Body[0] == '[' {
		jsonArray := []interface{}{}

		err := json.Unmarshal(r.Body, &jsonArray)
		if err == nil {
			f.crawlAPIDoc(c, r.Request.URL, jsonArray)
		}
	}
}
