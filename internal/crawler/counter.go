package crawler

import (
	log "github.com/sirupsen/logrus"
)

func counter(logMsg string, logInterval int) chan<- struct{} {
	ch := make(chan struct{})

	go func() {
		counter := 0

		for {
			_, ok := <-ch
			if !ok {
				return
			}

			counter++

			if counter%logInterval == 0 {
				log.WithField("count", counter).Info(logMsg)
			}
		}
	}()

	return ch
}
