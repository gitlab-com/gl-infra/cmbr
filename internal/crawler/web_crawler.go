package crawler

import (
	"errors"
	"net/url"
	"regexp"
	"strings"

	colly "github.com/gocolly/colly/v2"
	log "github.com/sirupsen/logrus"
)

var skipArchiveUrlsRegex = regexp.MustCompile(`\.zip|\.tar.gz|\.tar.bz2|\.tar`)
var skipUrlsRegex = regexp.MustCompile(`^/help/|/gitlab-qa-sandbox-group(-\d+)?/|/retry|/sign_out|` +
	`/follow.json|/cancel|/pause|/archive|/export|/housekeeping|/update_now|/import|` +
	`/play|/take_ownership|/clear_cache|/users/auth/|/revoke|/gitlab-qa-user(\d+)?/|/reset|` +
	`/impersonate|/disable_two_factor|/remove|/active_sessions|` +
	`/toggle_group_runners|/buy_minutes|.atom`)
var gitlabForksRegex = regexp.MustCompile(`/gitlab|/git-lab|/gitlab-ee|/gitlab-foss|/gitlab-ce`)
var projectsAPIRegex = regexp.MustCompile(`\/api\/v4\/projects\?page=\d+$`)
var projectAPIPathRegex = regexp.MustCompile(`\/api\/v4\/projects\/\d+$`)
var projectGitPathRegex = regexp.MustCompile(`\.git\/info`)
var projectRegistryRegex = regexp.MustCompile(`(packages|_registry)`)

func (f *crawler) addRelative(c *colly.Collector, baseURL *url.URL, relative string) {
	r, err := url.Parse(relative)
	if err != nil {
		return
	}

	u := baseURL.ResolveReference(r)
	f.addLink(c, u)
}

func (f *crawler) checkLink(c *colly.Collector, u *url.URL) bool {
	skipURL := false

	if f.options.Traffic == api {
		if !strings.HasPrefix(u.Path, "/api/v4/") {
			return true
		}
	}

	// Only add repository urls and paginated projects API endpoints
	if f.options.Traffic == git {
		if !projectAPIPathRegex.MatchString(u.Path) && !projectGitPathRegex.MatchString(u.Path) && !projectsAPIRegex.MatchString(u.String()) {
			return true
		}
	}

	// Only add project urls and paginated projects API endpoints
	if f.options.Traffic == registry {
		if !projectAPIPathRegex.MatchString(u.Path) && !projectRegistryRegex.MatchString(u.Path) && !projectsAPIRegex.MatchString(u.String()) {
			return true
		}
	}

	// Ignore help docs and other unrelated URLs
	if skipUrlsRegex.MatchString(u.Path) {
		return true
	}

	// Ignore url ends with Archive
	if skipArchiveUrlsRegex.MatchString(u.Path) {
		return true
	}

	// Reduce noise from known bug https://gitlab.com/gitlab-org/gitlab/-/issues/353163
	if gitlabForksRegex.MatchString(u.Path) {
		if strings.Contains(u.Path, "/access_tokens") {
			return true
		}
	}

	return skipURL
}

func (f *crawler) addLink(c *colly.Collector, u *url.URL) {
	if u.Scheme != "https" {
		return
	}

	skipURL := f.checkLink(c, u)

	if skipURL {
		return
	}

	log.WithField("url", u.String()).Debug("Adding link")

	err := c.Visit(u.String())
	if err != nil {
		if errors.Is(err, colly.ErrAlreadyVisited) {
			return
		}

		if errors.Is(err, colly.ErrForbiddenDomain) {
			return
		}

		if errors.Is(err, colly.ErrForbiddenURL) {
			return
		}

		log.WithError(err).WithField("url", u.String()).Info("failed to visit link")
	}
}
