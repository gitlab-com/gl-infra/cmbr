package authenticator

import (
	"context"
	b64 "encoding/base64"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"os"
	"strings"

	log "github.com/sirupsen/logrus"
	"golang.org/x/net/html"
)

// ErrLoginFailed is returned when the login fails.
var ErrLoginFailed = fmt.Errorf("login failed")

// Workaround to sign in as user and get its `_gitlab_session` cookie
// until https://gitlab.com/gitlab-org/gitlab/-/issues/27271 is implemented.
func getGitlabSessionCookie(header http.Header) string {
	var gitlabSessionCookie string

	for name, values := range header {
		for _, value := range values {
			if name == "Set-Cookie" && strings.Contains(value, "_gitlab_session") {
				gitlabSessionCookie = value
			}
		}
	}

	return gitlabSessionCookie
}

func findToken(n *html.Node) string {
	if n.Type == html.ElementNode && n.Data == "meta" {
		for _, a := range n.Attr {
			if a.Key == "name" && a.Val == "csrf-token" {
				token := n.Attr[1].Val
				return token
			}
		}
	}

	for c := n.FirstChild; c != nil; c = c.NextSibling {
		result := findToken(c)
		if result != "" {
			return result
		}
	}

	return ""
}

func performLoginExchange(signInURL string, hostURL string, username string, password string, csrfToken string, gitlabSessionCookie string) (string, error) {
	// Send authorize POST request using CSRF token and cookie from initial request
	data := url.Values{
		"user[login]":        {username},
		"user[password]":     {password},
		"authenticity_token": {csrfToken},
	}

	// Disable redirect to save response GitLab session cookie for authorized user
	client := &http.Client{
		CheckRedirect: func(req *http.Request, via []*http.Request) error {
			return http.ErrUseLastResponse
		},
	}

	req, err := http.NewRequestWithContext(context.Background(), http.MethodPost, signInURL, strings.NewReader(data.Encode()))
	if err != nil {
		return "", fmt.Errorf("unable to create sign-in POST request: %w", err)
	}

	req.Header.Set("User-Agent", os.Getenv("GITLAB_USER_AGENT"))
	req.Header.Set("Cookie", gitlabSessionCookie)
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")

	resp, err := client.Do(req)
	if err != nil {
		return "", fmt.Errorf("sign-in POST failed: %w", err)
	}
	defer resp.Body.Close()

	gitlabSessionCookie = getGitlabSessionCookie(resp.Header)

	return gitlabSessionCookie, nil
}

func confirmLogin(hostURL string, username string, gitlabSessionCookie string) error {
	// Confirm that cookie works
	req, err := http.NewRequestWithContext(context.Background(), http.MethodGet, hostURL, nil)
	if err != nil {
		return fmt.Errorf("failed to create GET request: %w", err)
	}

	req.Header.Set("User-Agent", os.Getenv("GITLAB_USER_AGENT"))
	req.Header.Set("Cookie", gitlabSessionCookie)

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return fmt.Errorf("failed to GET home page: %w", err)
	}

	defer resp.Body.Close()

	if resp.StatusCode == http.StatusOK {
		bodyBytes, err := io.ReadAll(resp.Body)
		if err != nil {
			return fmt.Errorf("failed to read body of response: %w", err)
		}

		bodyString := string(bodyBytes)

		// Confirm that with gitlabSessionCookie set we see username authorized
		auth := strings.Contains(bodyString, username)

		if !auth { //
			return fmt.Errorf("failed to authorize as '%s': %w", username, ErrLoginFailed)
		}
	}

	log.WithFields(log.Fields{"host": hostURL, "username": username}).Info("authorized")

	return nil
}

// SignIn authenticates the user and returns the GitLab session cookie.
func SignIn(hostURL string, username string, password string) (string, error) {
	signInURL := fmt.Sprintf("%s/users/sign_in", hostURL)

	req, err := http.NewRequestWithContext(context.Background(), http.MethodGet, signInURL, nil)
	if err != nil {
		return "", fmt.Errorf("failed to create GET request: %w", err)
	}

	req.Header.Set("User-Agent", os.Getenv("GITLAB_USER_AGENT"))

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return "", fmt.Errorf("unable to GET sign-in page: %w", err)
	}

	defer resp.Body.Close()

	doc, err := html.Parse(resp.Body)
	if err != nil {
		return "", fmt.Errorf("unable to parse body: %w", err)
	}

	csrfToken := findToken(doc)
	gitlabSessionCookie := getGitlabSessionCookie(resp.Header)

	gitlabSessionCookie, err = performLoginExchange(signInURL, hostURL, username, password, csrfToken, gitlabSessionCookie)
	if err != nil {
		return "", err
	}

	err = confirmLogin(hostURL, username, gitlabSessionCookie)
	if err != nil {
		return "", err
	}

	return gitlabSessionCookie, nil
}

// GenerateBasicAuth creates Basic Auth token for Git requests.
func GenerateBasicAuth(traffic string, username string, password string) string {
	basicAuth := ""

	if traffic != "git" || username == "" && password == "" {
		return basicAuth
	}

	auth := username + ":" + password
	basicAuth = fmt.Sprintf("Basic %s", b64.StdEncoding.EncodeToString([]byte(auth)))

	return basicAuth
}

func CheckInstance(hostURL string) error {
	req, err := http.NewRequestWithContext(context.Background(), http.MethodGet, hostURL, nil)
	if err != nil {
		return fmt.Errorf("failed to create GET request: %w", err)
	}

	// Required for Staging otherwise will fail with 403
	// due to CLoudFlare check
	req.Header.Set("User-Agent", os.Getenv("GITLAB_USER_AGENT"))

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		log.WithFields(log.Fields{"host": hostURL, "err": err}).Fatalf("failed to GET request instance host")
	}

	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		log.WithFields(log.Fields{"host": hostURL, "response code": resp.StatusCode}).Fatalf("failed to visit instance")
	}

	log.WithFields(log.Fields{"host": hostURL}).Info("instance is available")

	if err != nil {
		return fmt.Errorf("failed to check instance availability: %w", err)
	}

	return nil
}
