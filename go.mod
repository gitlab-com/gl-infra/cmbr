module gitlab.com/gitlab-com/gl-infra/cmbr

go 1.24.1

require (
	github.com/gocolly/colly/v2 v2.1.0
	github.com/sirupsen/logrus v1.9.3
	github.com/tomnomnom/linkheader v0.0.0-20180905144013-02ca5825eb80
	gitlab.com/gitlab-org/labkit v1.22.0
	golang.org/x/net v0.37.0
)

require (
	github.com/PuerkitoBio/goquery v1.8.0 // indirect
	github.com/andybalholm/cascadia v1.3.1 // indirect
	github.com/antchfx/htmlquery v1.2.5 // indirect
	github.com/antchfx/xmlquery v1.3.11 // indirect
	github.com/antchfx/xpath v1.2.1 // indirect
	github.com/gobwas/glob v0.2.3 // indirect
	github.com/golang/groupcache v0.0.0-20210331224755-41bb18bfe9da // indirect
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/kennygrant/sanitize v1.2.4 // indirect
	github.com/oklog/ulid/v2 v2.1.0 // indirect
	github.com/saintfish/chardet v0.0.0-20120816061221-3af4cd4741ca // indirect
	github.com/sebest/xff v0.0.0-20210106013422-671bd2870b3a // indirect
	github.com/temoto/robotstxt v1.1.2 // indirect
	gitlab.com/gitlab-org/go/reopen v1.0.0 // indirect
	golang.org/x/sys v0.31.0 // indirect
	golang.org/x/text v0.23.0 // indirect
	google.golang.org/appengine v1.6.7 // indirect
	google.golang.org/protobuf v1.34.2 // indirect
)
